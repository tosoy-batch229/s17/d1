console.log("Hello World!");

function printName() {
    console.log("My name is John");
}

printName();

// Declaration vs Expression
// This is a declared function.
function declaredFunction () {
    console.log("Hello World from declaredFunction()");
}

declaredFunction();

// function expression is when a function is stored in a variable.

// Anonymous functions are functions without names.

// variableFunction();
/* 
    error - function expression being stored in a let or const, cannot be hoisted.
*/

// Anonymous Function
// Function Expression Example
let variableFunction = function() {
    console.log("Hello Again!");
}

variableFunction();

let functionExpression = function functionName() {
    console.log("Hello from the other side!");
}

functionExpression();

// re-assigning a declared function into a new anonymous function.
declaredFunction = function() {
    console.log("updated declaredFunction()");
}

declaredFunction();

functionExpression = function() {
    console.log("updated functionExpression()");
}

functionExpression();

// It is not possible to re-assign a function initialized with "const".


/* 
    Function Scoping

    1. Local/Block Scope
    2. Global Scope
    3. Function
*/

{
    // This is a local variable and its value is accessible only inside the curly braces.
    let localVariable = "Armando Perez";
    console.log(localVariable);
}

// This is a global variable and its value is accessible anywhere in the code base.
let globalVariable = "Mr. WorldWide";

console.log(globalVariable);

function showNames() {
    // Function scope variables
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    // variables stored inside a function cannot be called outside the function.
    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();

// Nested Function
function myNewFunction() {
    let name = "Jane";

    function nestedFunction() {
        let nestedName = "John";

        console.log(name);
    }

    nestedFunction();

    // console.log(nestedName); -> error/ function scope.
}

myNewFunction();
// nestedFunction(); -> error/ local function

// Function and Global Scoped Variables
let globalName = "Alexandro";

function myNewFunction2() {
    let nameInside = "Renz";

    console.log(globalName);
}

myNewFunction2();
// console.log(nameInside); -> error/ function scope

// Using Alerts
// alerts() allows us to show small window at the top of our browser.
// alert("Hello World!");

// function showSampleAlert() {
//     alert("Hello User");
// }

// showSampleAlert();

// console.log("I will only log in the console when alert() is dismissed.");

// Notes on the use of alert() - it only show alert() for short dialog message. Don't overuse alert() because the program has to wait for it to be dismissed before continuing.

// Using Prompts
// prompts() allows us to show a small window and gather user input. Usually prompt() are stored inside a variable. It automatically converts the gathered data into a string.

// let samplePrompt = prompt("Enter Name: ");
// console.log("Hello, " + samplePrompt);
// console.log(typeof samplePrompt);

//let sampleNullPrompt = prompt("Don't enter anything."); -> returns an empty string.

function printWelcomeMessage() {
    let firstName = prompt("Enter your First Name: ");
    let lastName = prompt("Enter your Last Name: ");

    console.log(`Hello, ${firstName} ${lastName}!`);
    console.log("Welcome to my page!");
}

printWelcomeMessage();

/* 
    Function Naming Conventions

    Function names should be definitive of the task it will perform. It usually contains verb.
    Avoid generic names to avoid confusion within your code.
*/

function getCourses() {
    let courses = ["Science 101", "Math 101", "English 101"];

    console.log(courses);
}

getCourses();